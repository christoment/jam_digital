#include "LCD_support.h"

// convert from integer form to character sequence form
void convertIntToChar(unsigned int number, char out[]) {
	unsigned int val;
	unsigned int i;
	unsigned int maxNumber = strlen(out);
	char tmp;

	for (i = 0; i < maxNumber; i++) {
		val = number % 10;
		number = number / 10;

		switch (val) {
			case 1 : tmp = '1'; break;
			case 2 : tmp = '2'; break;
			case 3 : tmp = '3'; break;
			case 4 : tmp = '4'; break;
			case 5 : tmp = '5'; break;
			case 6 : tmp = '6'; break;
			case 7 : tmp = '7'; break;
			case 8 : tmp = '8'; break;
			case 9 : tmp = '9'; break;
			default : tmp = '0'; break;
		}

		out[maxNumber - i - 1] = tmp;
	}
}
