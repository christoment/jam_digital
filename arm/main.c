/*
 Praktikum Embedded 3
 Jam digital

 Anggota Kelompok :
 Adityo Prabowo 		/ 13211106
 Clement Christopher 	/ 13211138
 Misly Juliani 			/ 13211151

 Spesifikasi :
 - menggunakan task delay untuk tick waktu
 - menggunakan Keypad untuk tombol SET dan MODE
 - menggunakan LCD sebagai tampilan utama
 */

#include <OsConfig.h>
#include <CoOS.h>

#include <stdio.h>
#include "NUC1xx.h"
#include "DrvSYS.h"
#include "DrvGPIO.h"
#include "LCD.h"
#include "Scankey.h"

#include "LCD_support.h"
#include "simple_time.h"
#include "state_manager.h"
#include "time_manager.h"
#include "set_manager.h"
#include "state.h"
#include "press_manager.h"

// LCD PURPOSES
// --- TICK MODE
#define CHAR_WIDTH 8
#define LINE_HEIGHT 16
#define YEAR_LINE (0 * LINE_HEIGHT)
#define MONTH_LINE (0 * LINE_HEIGHT)
#define DAY_LINE (0 * LINE_HEIGHT)
#define HOUR_LINE (1 * LINE_HEIGHT)
#define MIN_LINE (1 * LINE_HEIGHT)
#define SEC_LINE (1 * LINE_HEIGHT)
#define YEAR_POS 0
#define MONTH_POS YEAR_POS + (CHAR_WIDTH * 5)
#define DAY_POS MONTH_POS + (CHAR_WIDTH * 3)
#define HOUR_POS 0
#define MIN_POS HOUR_POS + (CHAR_WIDTH * 3)
#define SEC_POS MIN_POS + (CHAR_WIDTH * 3)

#define MONTH_DATE_SEPARATOR_LINE (0 * LINE_HEIGHT)
#define MONTH_DATE_SEPARATOR_POS (DAY_POS - CHAR_WIDTH)
#define YEAR_MONTH_SEPARATOR_LINE (0 * LINE_HEIGHT)
#define YEAR_MONTH_SEPARATOR_POS (MONTH_POS - CHAR_WIDTH)
#define HOUR_MIN_SEPARATOR_LINE (1 * LINE_HEIGHT)
#define HOUR_MIN_SEPARATOR_POS (MIN_POS - CHAR_WIDTH)
#define MIN_SEC_SEPARATOR_LINE (1 * LINE_HEIGHT)
#define MIN_SEC_SEPARATOR_POS (SEC_POS - CHAR_WIDTH)

#define DATE_SEPARATOR "/"
#define TIME_SEPARATOR "."

// KEYPAD
#define KEY_MODE 1
#define KEY_SET 2
#define DELAY_HOLD_INCREMENT 0
#define HOLD_COUNTER_THRESHOLD 10

// RTOS
#define DELAY_LCD_SCAN 		CoTimeDelay(0,0,0,50)
#define DELAY_TICK 			CoTimeDelay(0,0,1,0)
#define DELAY_KEYPAD_SCAN 	CoTimeDelay(0,0,0,50)
#define DELAY_MANAGE_STATE 	CoTimeDelay(0,0,0,40)

OS_STK taskTick_stk[128]; 			// tick task
OS_STK taskKeypadScan_stk[128]; 	// tick scan and display
OS_STK taskDisplay_stk[128]; 		// tick scan and display
OS_STK taskManageState_stk[128]; 	// tick scan and display

OS_FlagID tickEnable_flag; 			// tick enabling flag
OS_FlagID dateEditPermission_flag; 	// date permission flag
OS_FlagID stateUpdated_flag; 		// date permission flag

// system date & time
// alokasi memori untuk char tahun, bulan, tanggal, jam, menit, detik
char year[] = "2014";
char month[] = "12";
char day[] = "31";
char hour[] = "24";
char min[] = "59";
char sec[] = "59";

// animation
volatile unsigned char tickAnimationCycle = 1;
volatile unsigned char buttonOnHold = 0;

// inisialisasi waktu
void initDate(void) {
	Date date;

	date.year = 2014;
	date.month = 10;
	date.day = 30;
	date.hour = 1;
	date.minute = 10;
	date.second = 0;

	setDate(date);
}

/*** RTOS TASKS ***/
// Timer for interrupt handler
void taskTick(void * ptr) {
	StatusType tickStat;

	for (;;) {
		// update parameter waktu
		tickStat = CoAcceptSingleFlag(tickEnable_flag);
		if (tickStat == E_OK) {
			CoSchedLock();
			CoClearFlag(dateEditPermission_flag);	// disable date edit permission flag
			updateTick();
			CoSetFlag(dateEditPermission_flag);		// enable flag back
			CoSchedUnlock();

			CoSetFlag(stateUpdated_flag); // set updated flag
		}

		// update tickAnimationCycle
		tickAnimationCycle = !tickAnimationCycle;

		DELAY_TICK; // delay 1 s
	}
}

void taskDisplay(void * ptr) {
	// Inisialisasi dan clear LCD
	init_LCD();
	DrvGPIO_ClrBit(E_GPD, 14); // backlight LCD on
	clear_LCD();

	for (;;) {
		// handles the state according to pressed button
		if (getMainState() == MODE_TICK) {
			clear_LCD();
			CoSetFlag(tickEnable_flag);

			// parse dari unsigned int ke karakter
			convertIntToChar(getYear(), year);
			convertIntToChar(getMonth(), month);
			convertIntToChar(getDay(), day);
			convertIntToChar(getHour(), hour);
			convertIntToChar(getMinute(), min);
			convertIntToChar(getSecond(), sec);

			// print waktu
			printS(YEAR_POS, YEAR_LINE, year);
			printS(MONTH_POS, MONTH_LINE, month);
			printS(DAY_POS, DAY_LINE, day);
			printS(HOUR_POS, HOUR_LINE, hour);
			printS(MIN_POS, MIN_LINE, min);
			printS(SEC_POS, SEC_LINE, sec);

			// print separator
			printS(MONTH_DATE_SEPARATOR_POS, MONTH_DATE_SEPARATOR_LINE,
					DATE_SEPARATOR);
			printS(YEAR_MONTH_SEPARATOR_POS, YEAR_MONTH_SEPARATOR_LINE,
					DATE_SEPARATOR);
			if (tickAnimationCycle) {
				printS(HOUR_MIN_SEPARATOR_POS, HOUR_MIN_SEPARATOR_LINE,
						TIME_SEPARATOR);
				printS(MIN_SEC_SEPARATOR_POS, MIN_SEC_SEPARATOR_LINE,
						TIME_SEPARATOR);
			}
		} else {
			// on mode set
			clear_LCD(); // clear LCD
			CoClearFlag(tickEnable_flag);

			// get every format first and also print time
			convertIntToChar(getYear(), year);
			convertIntToChar(getMonth(), month);
			convertIntToChar(getDay(), day);
			convertIntToChar(getHour(), hour);
			convertIntToChar(getMinute(), min);
			convertIntToChar(getSecond(), sec);

			// select which LCD format should be displayed according to
			// the setter state
			switch (getSetterDisplayState()) {
				case SET_SEC:
					printS(YEAR_POS, YEAR_LINE, year);
					printS(MONTH_POS, MONTH_LINE, month);
					printS(DAY_POS, DAY_LINE, day);
					printS(HOUR_POS, HOUR_LINE, hour);
					printS(MIN_POS, MIN_LINE, min);
					printS(SEC_POS, SEC_LINE, (tickAnimationCycle || buttonOnHold) ? sec : "  ");
					break;
				case SET_MIN:
					printS(YEAR_POS, YEAR_LINE, year);
					printS(MONTH_POS, MONTH_LINE, month);
					printS(DAY_POS, DAY_LINE, day);
					printS(HOUR_POS, HOUR_LINE, hour);
					printS(MIN_POS, MIN_LINE, (tickAnimationCycle || buttonOnHold) ? min : "  ");
					printS(SEC_POS, SEC_LINE, sec);
					break;
				case SET_HOUR:
					printS(YEAR_POS, YEAR_LINE, year);
					printS(MONTH_POS, MONTH_LINE, month);
					printS(DAY_POS, DAY_LINE, day);
					printS(HOUR_POS, HOUR_LINE, (tickAnimationCycle || buttonOnHold) ? hour : "  ");
					printS(MIN_POS, MIN_LINE, min);
					printS(SEC_POS, SEC_LINE, sec);
					break;
				case SET_DAY:
					printS(YEAR_POS, YEAR_LINE, year);
					printS(MONTH_POS, MONTH_LINE, month);
					printS(DAY_POS, DAY_LINE, (tickAnimationCycle || buttonOnHold) ? day : "  ");
					printS(HOUR_POS, HOUR_LINE, hour);
					printS(MIN_POS, MIN_LINE, min);
					printS(SEC_POS, SEC_LINE, sec);
					break;
				case SET_MNTH:
					printS(YEAR_POS, YEAR_LINE, year);
					printS(MONTH_POS, MONTH_LINE, (tickAnimationCycle || buttonOnHold) ? month : "  ");
					printS(DAY_POS, DAY_LINE, day);
					printS(HOUR_POS, HOUR_LINE, hour);
					printS(MIN_POS, MIN_LINE, min);
					printS(SEC_POS, SEC_LINE, sec);
					break;
				case SET_YEAR:
					printS(YEAR_POS, YEAR_LINE, (tickAnimationCycle || buttonOnHold) ? year : "    ");
					printS(MONTH_POS, MONTH_LINE, month);
					printS(DAY_POS, DAY_LINE, day);
					printS(HOUR_POS, HOUR_LINE, hour);
					printS(MIN_POS, MIN_LINE, min);
					printS(SEC_POS, SEC_LINE, sec);
					break;
			}

			// print separator
			printS(MONTH_DATE_SEPARATOR_POS, MONTH_DATE_SEPARATOR_LINE,
					DATE_SEPARATOR);
			printS(YEAR_MONTH_SEPARATOR_POS, YEAR_MONTH_SEPARATOR_LINE,
					DATE_SEPARATOR);
			printS(HOUR_MIN_SEPARATOR_POS, HOUR_MIN_SEPARATOR_LINE,
					TIME_SEPARATOR);
			printS(MIN_SEC_SEPARATOR_POS, MIN_SEC_SEPARATOR_LINE,
					TIME_SEPARATOR);
		}

		// print mode guide
		print_Line(3, "MODE  SET");

		DELAY_LCD_SCAN;
	}
}

void taskKeypadScan(void * ptr) {
	// Keypad
	int8_t number; 						// Variabel untuk menyimpan nomor keypad yang ditekan
	unsigned char disablePad = 0;
	unsigned char lastPad = 0; 			// anti repeating
	unsigned short int holdCounter = 0;
	unsigned short int incrementDelayCounter = 0;

	// Inisialisasi Keypad, menggunakan fungsi yang sudah ada di library utama
	OpenKeyPad(); // initialize 3x3 keypad

	// inisialisasi waktu pertama kali
	initDate();
	CoSetFlag(tickEnable_flag);

	for (;;) {
		// scan keypads - searching for MODE or SET button press
		number = ScanKey(); // get pressed keypad index

		if (lastPad != number) {
			holdCounter = 0;
			lastPad = -1;
			disablePad = 0;
			incrementDelayCounter = 0;
			buttonOnHold = 0;
		} else if (holdCounter > HOLD_COUNTER_THRESHOLD) {
			if (incrementDelayCounter > DELAY_HOLD_INCREMENT) {
				disablePad = 0;
				incrementDelayCounter = 0;
			} else {
				disablePad = 1;
				incrementDelayCounter++;
			}
		} else {
			buttonOnHold = 1;
			holdCounter++;
			disablePad = 1;
		}

		if (!disablePad) {
			switch (number) {
				case KEY_MODE:
					tickAnimationCycle = 0; // set in order to give hint for new selection
					lastPad = KEY_MODE; // de-repeating

					modePressed();

					CoSetFlag(stateUpdated_flag); // set updated flag
					break;
				case KEY_SET:
					tickAnimationCycle = 1; // set in order to make new time appeared instantly
					lastPad = KEY_SET; // de-repeating

					setPressed();

					CoSetFlag(stateUpdated_flag); // set updated flag
					break;
				}
		}

		DELAY_KEYPAD_SCAN;
	}
}

void taskManageState(void * ptr) {
	StatusType isUpdatedStat;
	StatusType permissionStat;
	for (;;) {
		isUpdatedStat = CoWaitForSingleFlag(stateUpdated_flag, 0);

		if (isUpdatedStat == E_OK) {
			permissionStat = CoWaitForSingleFlag(dateEditPermission_flag, 0);

			if (permissionStat == E_OK) {
				// update state
				startManageState(getDatePointer());

				// return updated flag to not set
				CoClearFlag(stateUpdated_flag);
			}
		}

		DELAY_MANAGE_STATE;
	}
}

void initFlag(void) {
	tickEnable_flag = CoCreateFlag(0, 0); 		// initiate tick enable flag, default off
	dateEditPermission_flag = CoCreateFlag(0, 0); 	// initiate date permission flag, default off
	stateUpdated_flag = CoCreateFlag(0, 0); 	// initiate date permission flag, default off
}

int main(void) {
	// Set clock yang digunakan sistem
	UNLOCKREG();
	DrvSYS_Open(50000000); // set MCU to run at 50MHz
	LOCKREG();

	CoInitOS(); // init CoOS

	initFlag();

	CoCreateTask(taskTick, 0, 0, &taskTick_stk[128-1], 128);				// periodic tick task
	CoCreateTask(taskKeypadScan, 0, 1, &taskKeypadScan_stk[128-1], 128);	// keypad scanning task
	CoCreateTask(taskDisplay, 0, 2, &taskDisplay_stk[128-1], 128);			// display (LCD) scanning task
	CoCreateTask(taskManageState, 0, 2, &taskManageState_stk[128-1], 128);	// state manager task

	CoStartOS();

	// prevention?
	while (1) {
	}
}

