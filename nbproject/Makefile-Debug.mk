#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/common/press_manager.o \
	${OBJECTDIR}/common/set_manager.o \
	${OBJECTDIR}/common/simple_time.o \
	${OBJECTDIR}/common/state.o \
	${OBJECTDIR}/common/state_manager.o \
	${OBJECTDIR}/common/time_manager.o \
	${OBJECTDIR}/test-desktop/main.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1

# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lcunit

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${TESTDIR}/TestFiles/f2.exe

${TESTDIR}/TestFiles/f2.exe: ${OBJECTFILES}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.c} -o ${TESTDIR}/TestFiles/f2 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/common/press_manager.o: common/press_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/press_manager.o common/press_manager.c

${OBJECTDIR}/common/set_manager.o: common/set_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/set_manager.o common/set_manager.c

${OBJECTDIR}/common/simple_time.o: common/simple_time.c 
	${MKDIR} -p ${OBJECTDIR}/common
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/simple_time.o common/simple_time.c

${OBJECTDIR}/common/state.o: common/state.c 
	${MKDIR} -p ${OBJECTDIR}/common
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/state.o common/state.c

${OBJECTDIR}/common/state_manager.o: common/state_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/state_manager.o common/state_manager.c

${OBJECTDIR}/common/time_manager.o: common/time_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/time_manager.o common/time_manager.c

${OBJECTDIR}/test-desktop/main.o: test-desktop/main.c 
	${MKDIR} -p ${OBJECTDIR}/test-desktop
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/test-desktop/main.o test-desktop/main.c

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-conf ${TESTFILES}
${TESTDIR}/TestFiles/f1: ${TESTDIR}/unit-test/test_common_main.o ${TESTDIR}/unit-test/test_simple_time.o ${TESTDIR}/unit-test/test_state.o ${TESTDIR}/unit-test/test_time_manager.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.c}   -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS} -lcunit 


${TESTDIR}/unit-test/test_common_main.o: unit-test/test_common_main.c 
	${MKDIR} -p ${TESTDIR}/unit-test
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit-test/test_common_main.o unit-test/test_common_main.c


${TESTDIR}/unit-test/test_simple_time.o: unit-test/test_simple_time.c 
	${MKDIR} -p ${TESTDIR}/unit-test
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit-test/test_simple_time.o unit-test/test_simple_time.c


${TESTDIR}/unit-test/test_state.o: unit-test/test_state.c 
	${MKDIR} -p ${TESTDIR}/unit-test
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit-test/test_state.o unit-test/test_state.c


${TESTDIR}/unit-test/test_time_manager.o: unit-test/test_time_manager.c 
	${MKDIR} -p ${TESTDIR}/unit-test
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${TESTDIR}/unit-test/test_time_manager.o unit-test/test_time_manager.c


${OBJECTDIR}/common/press_manager_nomain.o: ${OBJECTDIR}/common/press_manager.o common/press_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	@NMOUTPUT=`${NM} ${OBJECTDIR}/common/press_manager.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/press_manager_nomain.o common/press_manager.c;\
	else  \
	    ${CP} ${OBJECTDIR}/common/press_manager.o ${OBJECTDIR}/common/press_manager_nomain.o;\
	fi

${OBJECTDIR}/common/set_manager_nomain.o: ${OBJECTDIR}/common/set_manager.o common/set_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	@NMOUTPUT=`${NM} ${OBJECTDIR}/common/set_manager.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/set_manager_nomain.o common/set_manager.c;\
	else  \
	    ${CP} ${OBJECTDIR}/common/set_manager.o ${OBJECTDIR}/common/set_manager_nomain.o;\
	fi

${OBJECTDIR}/common/simple_time_nomain.o: ${OBJECTDIR}/common/simple_time.o common/simple_time.c 
	${MKDIR} -p ${OBJECTDIR}/common
	@NMOUTPUT=`${NM} ${OBJECTDIR}/common/simple_time.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/simple_time_nomain.o common/simple_time.c;\
	else  \
	    ${CP} ${OBJECTDIR}/common/simple_time.o ${OBJECTDIR}/common/simple_time_nomain.o;\
	fi

${OBJECTDIR}/common/state_nomain.o: ${OBJECTDIR}/common/state.o common/state.c 
	${MKDIR} -p ${OBJECTDIR}/common
	@NMOUTPUT=`${NM} ${OBJECTDIR}/common/state.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/state_nomain.o common/state.c;\
	else  \
	    ${CP} ${OBJECTDIR}/common/state.o ${OBJECTDIR}/common/state_nomain.o;\
	fi

${OBJECTDIR}/common/state_manager_nomain.o: ${OBJECTDIR}/common/state_manager.o common/state_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	@NMOUTPUT=`${NM} ${OBJECTDIR}/common/state_manager.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/state_manager_nomain.o common/state_manager.c;\
	else  \
	    ${CP} ${OBJECTDIR}/common/state_manager.o ${OBJECTDIR}/common/state_manager_nomain.o;\
	fi

${OBJECTDIR}/common/time_manager_nomain.o: ${OBJECTDIR}/common/time_manager.o common/time_manager.c 
	${MKDIR} -p ${OBJECTDIR}/common
	@NMOUTPUT=`${NM} ${OBJECTDIR}/common/time_manager.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/common/time_manager_nomain.o common/time_manager.c;\
	else  \
	    ${CP} ${OBJECTDIR}/common/time_manager.o ${OBJECTDIR}/common/time_manager_nomain.o;\
	fi

${OBJECTDIR}/test-desktop/main_nomain.o: ${OBJECTDIR}/test-desktop/main.o test-desktop/main.c 
	${MKDIR} -p ${OBJECTDIR}/test-desktop
	@NMOUTPUT=`${NM} ${OBJECTDIR}/test-desktop/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.c) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/test-desktop/main_nomain.o test-desktop/main.c;\
	else  \
	    ${CP} ${OBJECTDIR}/test-desktop/main.o ${OBJECTDIR}/test-desktop/main_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${TESTDIR}/TestFiles/f2.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
