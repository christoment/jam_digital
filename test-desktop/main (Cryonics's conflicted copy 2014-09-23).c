/*
 * main.c
 *
 *  Created on: 15 Sep 2014
 *      Author: Christ
 */

#include <stdio.h>
#include "../common/simple_time.h"
#include "../common/state_manager.h"
#include "../common/time_manager.h"
#include "../common/set_manager.h"
#include "../common/state.h"
#include "../common/press_manager.h"

#define EXIT 0
#define SET_TIME 1
#define PRESS_MODE 2
#define PRESS_SET 3
#define TICK 4

void main() {
    int choice = -1;
    Date date;

    printf("\n\n\nDESKTOP-TEST\n\n");

    while (choice != EXIT) {
        printf("\n\nCurrent time : %d-%d-%d %d:%d:%d", getYear(), getMonth(), getDay(), getHour(), getMinute(), getSecond());
        printf("\n    current main state : %d", getMainState());
        printf("\n    current set mode state : %d", getSetterDisplayState());
        printf("\n    current set trigger state : %d\n", getSetterTrigerState());
        printMenu();
        choice = getChoice();

        printf("\n");

        switch (choice) {
            case SET_TIME:
                printf("enter year : ");
                scanf("%u", &(date.year));
                printf("enter month : ");
                scanf("%u", &(date.month));
                printf("enter day : ");
                scanf("%u", &(date.day));
                printf("enter hour : ");
                scanf("%d", &(date.hour));
                printf("enter minute : ");
                scanf("%d", &(date.minute));
                printf("enter second : ");
                scanf("%d", &(date.second));

                setDate(date);
                break;
            case PRESS_MODE:
                modePressed();
                break;
            case PRESS_SET:
                setPressed();
                break;
            case TICK:
                updateTick();
                break;
        }

        startManageState();
        printf("\n");
    }
}

int getChoice() {
    int choice = -1;
    printf(">>");
    scanf("%d", &choice);

    return choice;
}

void printMenu() {
    printf("\n==============\n");
    printf("MENU\n");
    printf("==============\n\n");

    printf("1. Set Time\n");
    printf("2. Press MODE Button\n");
    printf("3. Press SET Button\n");
    printf("4. Tick\n");
    printf("\n0. Exit\n\n");
}
