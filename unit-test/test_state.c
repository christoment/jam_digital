/*
 * File:   test_state.c
 * Author: Christ
 *
 * Created on Sep 23, 2014, 5:07:55 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "../common/state.h"

#define TEST_STATE_CONSTANT 5

/*
 * CUnit Test Suite
 */

int init_suite_state(void) {
    return 0;
}

int clean_suite_state(void) {
    return 0;
}

void testGetMainState() {
    setMainState(TEST_STATE_CONSTANT);

    unsigned char result = getMainState();

    CU_ASSERT(result == TEST_STATE_CONSTANT);
}

void testGetSetterDisplayState() {
    setSetterDisplayState(TEST_STATE_CONSTANT);

    unsigned char result = getSetterDisplayState();

    CU_ASSERT(result == TEST_STATE_CONSTANT);
}

void testGetSetterTrigerState() {
    setSetterTriggerState(TEST_STATE_CONSTANT);

    unsigned char result = getSetterTriggerState();

    CU_ASSERT(result == TEST_STATE_CONSTANT);
}

int prepareTestState() {
    CU_pSuite pSuite = NULL;

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_state", init_suite_state, clean_suite_state);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "testGetMainState", testGetMainState)) ||
            (NULL == CU_add_test(pSuite, "testGetSetterDisplayState", testGetSetterDisplayState)) ||
            (NULL == CU_add_test(pSuite, "testGetSetterTrigerState", testGetSetterTrigerState))) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    return CU_get_error();
}
