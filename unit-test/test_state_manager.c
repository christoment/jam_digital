/*
 * File:   test_state_manager.c
 * Author: Christ
 *
 * Created on 25/09/2014, 10:03:18 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "../common/state_manager.h"
#include "../common/simple_time.h"
#include "../common/state.h"

/*
 * CUnit Test Suite
 */

int init_suite_state_manager(void) {
    return 0;
}

int clean_suite_state_manager(void) {
    return 0;
}

void testStartManageState() {
    setupState();
    
    Date date;
    date = newDate(1, 1, 1, 1, 1, 2001);
    
    // normal increase, main state should be on state 0
    startManageState(&date);
    CU_ASSERT_EQUAL(getMainState(), MODE_TICK);
    
    // test on other state, should persist on state 1
    setMainState(MODE_SET);
    startManageState(&date);
    CU_ASSERT_EQUAL(getMainState(), MODE_SET);
    
    // test on trigger state, trigger should changed to 0
    setSetterTriggerState(1);
    startManageState(&date);
    CU_ASSERT_EQUAL(getSetterTriggerState(), 0);
}

int prepareTestStateManager() {
    CU_pSuite pSuite = NULL;

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_state_manager", init_suite_state_manager, clean_suite_state_manager);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "testStartManageState", testStartManageState))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    return CU_get_error();
}
