/* 
 * File:   test_press_manager.h
 * Author: Christ
 *
 * Created on 25 September 2014, 10:27 PM
 */

#ifndef TEST_PRESS_MANAGER_H
#define	TEST_PRESS_MANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif

    int prepareTestPressManager();

#ifdef	__cplusplus
}
#endif

#endif	/* TEST_PRESS_MANAGER_H */

