/*
 * File:   test_set_manager.c
 * Author: Christ
 *
 * Created on 25/09/2014, 9:40:19 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "../common/set_manager.h"

/*
 * CUnit Test Suite
 */

int init_suite_set_manager(void) {
    return 0;
}

int clean_suite_set_manager(void) {
    return 0;
}

void testIncreaseday() {
    Date date;
    
    date.day = 1;
    date.month = 1;
    date.year = 2001;
    increaseDay(&date);
    CU_ASSERT_EQUAL(date.day, 2);
    
    // check normal end date
    date.day = 31;
    date.month = 1;
    date.year = 2001;
    increaseDay(&date);
    CU_ASSERT_EQUAL(date.day, 1);
    
    // check on feb
    date.day = 28;
    date.month = 2;
    date.year = 2001;
    increaseDay(&date);
    CU_ASSERT_EQUAL(date.day, 1);
    
    date.day = 28;
    date.month = 2;
    date.year = 2004;
    increaseDay(&date);
    CU_ASSERT_EQUAL(date.day, 29);
    increaseDay(&date);
    CU_ASSERT_EQUAL(date.day, 1);
}

void testIncreasehour() {
    Date date;
    
    date.hour = 1;
    increaseHour(&date);
    CU_ASSERT_EQUAL(date.hour, 2);
    
    date.hour = 23;
    increaseHour(&date);
    CU_ASSERT_EQUAL(date.hour, 0);
}

void testIncreaseminute() {
    Date date;
    
    date.minute = 1;
    increaseMinute(&date);
    CU_ASSERT_EQUAL(date.minute, 2);
    
    date.minute = 59;
    increaseMinute(&date);
    CU_ASSERT_EQUAL(date.minute, 0);
}

void testIncreasemonth() {
    Date date;
    
    date.month = 1;
    increaseMonth(&date);
    CU_ASSERT_EQUAL(date.month, 2);
    
    date.month = 12;
    increaseMonth(&date);
    CU_ASSERT_EQUAL(date.month, 1);
}

void testIncreasesecond() {
    Date date;
    
    date.second = 1;
    increaseSecond(&date);
    CU_ASSERT_EQUAL(date.second, 2);
    
    date.second = 59;
    increaseSecond(&date);
    CU_ASSERT_EQUAL(date.second, 0);
}

void testIncreaseyear() {
    Date date;
    
    date.year = 1;
    increaseYear(&date);
    CU_ASSERT_EQUAL(date.year, 2);
    
    date.year = 2000;
    increaseYear(&date);
    CU_ASSERT_EQUAL(date.year, 2001);
}

void testIncreaseParam() {
    unsigned int param;
    static const unsigned int MIN_VALUE = 1;
    static const unsigned int MAX_VALUE = 59;
    
    param = MAX_VALUE;
    increaseParam(&param, MIN_VALUE, MAX_VALUE);
    
    CU_ASSERT_EQUAL(param, MIN_VALUE);
}

int prepareTestSetManager() {
    CU_pSuite pSuite = NULL;

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_set_manager", init_suite_set_manager, clean_suite_set_manager);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "testIncreaseday", testIncreaseday)) ||
            (NULL == CU_add_test(pSuite, "testIncreasehour", testIncreasehour)) ||
            (NULL == CU_add_test(pSuite, "testIncreaseminute", testIncreaseminute)) ||
            (NULL == CU_add_test(pSuite, "testIncreasemonth", testIncreasemonth)) ||
            (NULL == CU_add_test(pSuite, "testIncreasesecond", testIncreasesecond)) ||
            (NULL == CU_add_test(pSuite, "testIncreaseyear", testIncreaseyear)) ||
            (NULL == CU_add_test(pSuite, "testIncreaseParam", testIncreaseParam))) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    return CU_get_error();
}
