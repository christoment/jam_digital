#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "test_state.h"
#include "test_simple_time.h"
#include "test_time_manager.h"
#include "test_set_manager.h"
#include "test_state_manager.h"
#include "test_press_manager.h"

int main() {
    /* Initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    /* Add a suite to the registry */
    prepareTestSimpleTime();
    prepareTestState();
    prepareTestTimeManager();
    prepareTestSetManager();
    prepareTestStateManager();
    prepareTestPressManager();

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}