/* 
 * File:   test_simple_time.h
 * Author: Christ
 *
 * Created on 24 September 2014, 8:00 PM
 */

#ifndef TEST_SIMPLE_TIME_H
#define	TEST_SIMPLE_TIME_H

#ifdef	__cplusplus
extern "C" {
#endif


    int prepareTestSimpleTime();

#ifdef	__cplusplus
}
#endif

#endif	/* TEST_SIMPLE_TIME_H */

