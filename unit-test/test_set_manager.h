/* 
 * File:   test_set_manager.h
 * Author: Christ
 *
 * Created on 25 September 2014, 9:40 PM
 */

#ifndef TEST_SET_MANAGER_H
#define	TEST_SET_MANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif

    int prepareTestSetManager();


#ifdef	__cplusplus
}
#endif

#endif	/* TEST_SET_MANAGER_H */

