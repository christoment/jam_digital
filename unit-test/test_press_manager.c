/*
 * File:   test_press_manager.c
 * Author: Christ
 *
 * Created on 25/09/2014, 10:19:42 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "../common/press_manager.h"
#include "../common/state_manager.h"

/*
 * CUnit Test Suite
 */

int init_suite_press_manager(void) {    
    return 0;
}

int clean_suite_press_manager(void) {
    return 0;
}

void testModePressed() {
    setupState();
    
    // current state should be on : MODE_TICK, SET_SEC
    // press mode to change to MODE_SET
    modePressed();
    CU_ASSERT_EQUAL(getMainState(), MODE_SET);
    
    // cycle through set mode : 
    // year -> month -> day -> hour -> min -> sec
    CU_ASSERT_EQUAL(getSetterDisplayState(), SET_YEAR);
    
    modePressed();
    CU_ASSERT_EQUAL(getSetterDisplayState(), SET_MNTH);
    
    modePressed();
    CU_ASSERT_EQUAL(getSetterDisplayState(), SET_DAY);
    
    modePressed();
    CU_ASSERT_EQUAL(getSetterDisplayState(), SET_HOUR);
    
    modePressed();
    CU_ASSERT_EQUAL(getSetterDisplayState(), SET_MIN);
    
    modePressed();
    CU_ASSERT_EQUAL(getSetterDisplayState(), SET_SEC);
    
    // on more press should go to MODE_TICK
    modePressed();
    CU_ASSERT_EQUAL(getMainState(), MODE_TICK);
}

void testSetPressed() {
    setupState();
    
    CU_ASSERT_EQUAL(getSetterTriggerState(), 0);
    // pressing set on MODE_SET should set TRIGGER state to 1
    setMainState(MODE_SET);
    setPressed();
    CU_ASSERT_EQUAL(getSetterTriggerState(), 1);
    
    setupState();
    CU_ASSERT_EQUAL(getSetterTriggerState(), 0);
    // pressing set on MODE_TICK should do nothing
    setMainState(MODE_TICK);
    setPressed();
    CU_ASSERT_EQUAL(getSetterTriggerState(), 0);
}

int prepareTestPressManager() {
    CU_pSuite pSuite = NULL;

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_press_manager", init_suite_press_manager, clean_suite_press_manager);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "testModePressed", testModePressed)) ||
            (NULL == CU_add_test(pSuite, "testSetPressed", testSetPressed))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    return CU_get_error();
}
