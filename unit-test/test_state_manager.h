/* 
 * File:   test_state_manager.h
 * Author: Christ
 *
 * Created on 25 September 2014, 10:13 PM
 */

#ifndef TEST_STATE_MANAGER_H
#define	TEST_STATE_MANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif


    int prepareTestStateManager();


#ifdef	__cplusplus
}
#endif

#endif	/* TEST_STATE_MANAGER_H */

