/*
 * File:   test_time_manager.c
 * Author: Christ
 *
 * Created on Sep 23, 2014, 5:40:20 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "../common/time_manager.h"

/*
 * CUnit Test Suite
 */
static const int TEST_DATE = 1;
static const int TEST_MONTH = 2;
static const int TEST_YEAR = 3;
static const int TEST_SECOND = 4;
static const int TEST_MINUTE = 5;
static const int TEST_HOUR = 6;

int init_suite_time_manager(void) {
    Date temp;

    temp = newDate(TEST_SECOND, TEST_MINUTE, TEST_HOUR, TEST_DATE, TEST_MONTH, TEST_YEAR);

    setDate(temp);

    return 0;
}

int clean_suite_time_manager(void) {
    return 0;
}

void testGetDay() {
    unsigned char result = getDay();
    
    CU_ASSERT_EQUAL(result, TEST_DATE);
}

void testGetHour() {
    unsigned char result = getHour();
    
    CU_ASSERT_EQUAL(result, TEST_HOUR);
}

void testGetMinute() {
    unsigned char result = getMinute();
    
    CU_ASSERT_EQUAL(result, TEST_MINUTE);
}

void testGetMonth() {
    unsigned char result = getMonth();
    
    CU_ASSERT_EQUAL(result, TEST_MONTH);
}

void testGetSecond() {
    unsigned char result = getSecond();
    
    CU_ASSERT_EQUAL(result, TEST_SECOND);
}

void testGetYear() {
    unsigned int result = getYear();
    
    CU_ASSERT_EQUAL(result, TEST_YEAR);
}

void testUpdateTick() {
    unsigned char testSecond = getSecond() + 1;
    
    updateTick();
    
    unsigned char result = getSecond();
    
    CU_ASSERT_EQUAL(result, testSecond);
}

int prepareTestTimeManager() {
    CU_pSuite pSuite = NULL;

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_time_manager", init_suite_time_manager, clean_suite_time_manager);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if (
            (NULL == CU_add_test(pSuite, "testGetDay", testGetDay)) ||
            (NULL == CU_add_test(pSuite, "testGetHour", testGetHour)) ||
            (NULL == CU_add_test(pSuite, "testGetMinute", testGetMinute)) ||
            (NULL == CU_add_test(pSuite, "testGetMonth", testGetMonth)) ||
            (NULL == CU_add_test(pSuite, "testGetSecond", testGetSecond)) ||
            (NULL == CU_add_test(pSuite, "testGetYear", testGetYear)) ||
            (NULL == CU_add_test(pSuite, "testUpdateTick", testUpdateTick))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    return CU_get_error();
}
