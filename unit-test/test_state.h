/* 
 * File:   test_state.h
 * Author: Christ
 *
 * Created on 24 September 2014, 7:05 PM
 */

#ifndef TEST_STATE_H
#define	TEST_STATE_H

#ifdef	__cplusplus
extern "C" {
#endif

    int prepareTestState();


#ifdef	__cplusplus
}
#endif

#endif	/* TEST_STATE_H */

