/* 
 * File:   test_time_manager.h
 * Author: Christ
 *
 * Created on 24 September 2014, 7:10 PM
 */

#ifndef TEST_TIME_MANAGER_H
#define	TEST_TIME_MANAGER_H

#ifdef	__cplusplus
extern "C" {
#endif

    int prepareTestTimeManager();


#ifdef	__cplusplus
}
#endif

#endif	/* TEST_TIME_MANAGER_H */

