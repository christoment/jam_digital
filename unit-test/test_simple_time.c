/*
 * File:   test_simple_time.c
 * Author: Christ
 *
 * Created on 24/09/2014, 7:52:45 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "../common/simple_time.h"

/*
 * CUnit Test Suite
 */

static const int TEST_DATE = 1;
static const int TEST_MONTH = 2;
static const int TEST_YEAR = 3;
static const int TEST_SECOND = 4;
static const int TEST_MINUTE = 5;
static const int TEST_HOUR = 6;

int init_suite_simple_time(void) {
    return 0;
}

int clean_suite_simple_time(void) {
    return 0;
}

void testCopyDate() {
    Date date;
    date.second = TEST_SECOND;
    date.minute = TEST_MINUTE;
    date.hour = TEST_HOUR;
    date.day = TEST_DATE;
    date.month = TEST_MONTH;
    date.year = TEST_YEAR;

    Date copiedDate;
    copyDate(&copiedDate, date);

    CU_ASSERT(
            (copiedDate.second == date.second) &&
            (copiedDate.minute == date.minute) &&
            (copiedDate.hour == date.hour) &&
            (copiedDate.day == date.day) &&
            (copiedDate.month == date.month) &&
            (copiedDate.year == date.year)
            );
}

void testIncreaseTime() {
    Date date;
    date.second = TEST_SECOND;
    date.minute = TEST_MINUTE;
    date.hour = TEST_HOUR;
    date.day = TEST_DATE;
    date.month = TEST_MONTH;
    date.year = TEST_YEAR;

    increaseTime(&date);

    CU_ASSERT_EQUAL(date.second, (TEST_SECOND + 1));
    
    date.second = 59;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.second, 0);
    CU_ASSERT_EQUAL(date.minute, (TEST_MINUTE + 1));
    
    date.second = 59;
    date.minute = 59;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.second, 0);
    CU_ASSERT_EQUAL(date.minute, 0);
    CU_ASSERT_EQUAL(date.hour, (TEST_HOUR + 1));
    
    date.second = 59;
    date.minute = 59;
    date.hour = 23;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.second, 0);
    CU_ASSERT_EQUAL(date.minute, 0);
    CU_ASSERT_EQUAL(date.hour, 0);
    CU_ASSERT_EQUAL(date.day, (TEST_DATE + 1));
    
    date.second = 59;
    date.minute = 59;
    date.hour = 23;
    date.day = 31;
    date.month = 1;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.second, 0);
    CU_ASSERT_EQUAL(date.minute, 0);
    CU_ASSERT_EQUAL(date.hour, 0);
    CU_ASSERT_EQUAL(date.day, 1);
    CU_ASSERT_EQUAL(date.month, 2);
    
    date.second = 59;
    date.minute = 59;
    date.hour = 23;
    date.day = 31;
    date.month = 12;
    date.year = TEST_YEAR;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.second, 0);
    CU_ASSERT_EQUAL(date.minute, 0);
    CU_ASSERT_EQUAL(date.hour, 0);
    CU_ASSERT_EQUAL(date.day, 1);
    CU_ASSERT_EQUAL(date.month, 1);
    CU_ASSERT_EQUAL(date.year, TEST_YEAR + 1);
    
    // test pada kejadian berkaitan dengan kabisat
    date.second = 59;
    date.minute = 59;
    date.hour = 23;
    date.day = 28;
    date.month = 2;
    date.year = 2003;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.day, 1);
    CU_ASSERT_EQUAL(date.month, 3);
    
    date.second = 59;
    date.minute = 59;
    date.hour = 23;
    date.day = 29;
    date.month = 2;
    date.year = 2004;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.day, 1);
    CU_ASSERT_EQUAL(date.month, 3);
    
    date.second = 59;
    date.minute = 59;
    date.hour = 23;
    date.day = 28;
    date.month = 2;
    date.year = 2010;
    increaseTime(&date);
    CU_ASSERT_EQUAL(date.day, 1);
    CU_ASSERT_EQUAL(date.month, 3);
}

void testNewDate() {
    Date date;
    Date craftedDate;
    
    date.second = TEST_SECOND;
    date.minute = TEST_MINUTE;
    date.hour = TEST_HOUR;
    date.day = TEST_DATE;
    date.month = TEST_MONTH;
    date.year = TEST_YEAR;
    
    
    craftedDate = newDate(TEST_SECOND, TEST_MINUTE, TEST_HOUR, TEST_DATE, TEST_MONTH, TEST_YEAR);
       
    CU_ASSERT(
            (craftedDate.second == date.second) &&
            (craftedDate.minute == date.minute) &&
            (craftedDate.hour == date.hour) &&
            (craftedDate.day == date.day) &&
            (craftedDate.month == date.month) &&
            (craftedDate.year == date.year)
            );
}

int prepareTestSimpleTime() {
    CU_pSuite pSuite = NULL;

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_simple_time", init_suite_simple_time, clean_suite_simple_time);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "testCopyDate", testCopyDate)) ||
            (NULL == CU_add_test(pSuite, "testIncreaseTime", testIncreaseTime)) ||
            (NULL == CU_add_test(pSuite, "testNewDate", testNewDate))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    return CU_get_error();
}
