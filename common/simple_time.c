/*
 * simple_time.c
 *
 *  Created on: Sep 12, 2014
 *      Author: Christ
 */

#include "simple_time.h"

Date newDate(unsigned char _second, unsigned char _minute, unsigned char _hour, unsigned char _day, unsigned char _month, unsigned int _year) {
    Date date;
    date.second = _second;
    date.minute = _minute;
    date.hour = _hour;
    date.day = _day;
    date.month = _month;
    date.year = _year;

    return date;
}

void copyDate(Date *_dateTarget, Date _dateSource) {
    (*_dateTarget).second = _dateSource.second;
    (*_dateTarget).minute = _dateSource.minute;
    (*_dateTarget).hour = _dateSource.hour;
    (*_dateTarget).day = _dateSource.day;
    (*_dateTarget).month = _dateSource.month;
    (*_dateTarget).year = _dateSource.year;
}

void increaseTime(Date *_date) {
	int dayOfMonth;
	(*_date).second++;

    if ((*_date).second >= 60) {
        (*_date).second %= 60;
        (*_date).minute++;

        if ((*_date).minute >= 60) {
            (*_date).minute %= 60;
            (*_date).hour++;

            if ((*_date).hour >= 24) {
                (*_date).hour %= 24;
                (*_date).day++;

                dayOfMonth = getDayOfMonthCount((*_date).month, (*_date).year);

                if ((*_date).day > dayOfMonth) {
                    (*_date).day %= dayOfMonth;
                    (*_date).month++;

                    if ((*_date).month > 12) {
                        (*_date).month %= 12;
                        (*_date).year++;
                    }
                }
            }
        }
    }
}

int getDayOfMonthCount(unsigned int _month, unsigned int _year) {
    // cek bulan
    switch (_month) {
        case JANUARY:
        case MARCH:
        case MAY:
        case JULY:
        case AUGUST:
        case OCTOBER:
        case DECEMBER:
            return 31;
        case FEBRUARY:
            // cek tahun kabisat
            if ((_year % 4 == 0) && (_year % 10 != 0))
                return 29;
            else
                return 28;
        default:
            return 30;
    }
}
