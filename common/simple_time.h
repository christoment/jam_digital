/*
 * simple_time.h
 *
 *  Created on: Sep 12, 2014
 *      Author: Christ
 */

#ifndef SIMPLETIME_H_
#define SIMPLETIME_H_

typedef struct {
	unsigned int hour;
	unsigned int minute;
	unsigned int second;
	unsigned int day;
	unsigned int month;
	unsigned int year;
} Date;

typedef enum {
	JANUARY = 1,
	FEBRUARY = 2,
	MARCH = 3,
	APRIL = 4,
	MAY = 5,
	JUNE = 6,
	JULY = 7,
	AUGUST = 8,
	SEPTEMBER = 9,
	OCTOBER = 10,
	NOVEMBER = 11,
	DECEMBER = 12
} Month;

Date newDate(unsigned char _second, unsigned char _minute, unsigned char _hour, unsigned char _day, unsigned char _month, unsigned int _year);
void copyDate(Date *_dateTarget, Date _dateSource);
void increaseTime(Date *_date);
int getDayOfMonthCount(unsigned int _month, unsigned int _year);

#endif /* SIMPLETIME_H_ */
