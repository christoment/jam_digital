#ifndef PRESS_MANAGER_H_INCLUDED
#define PRESS_MANAGER_H_INCLUDED

#include "state.h"
#include "set_manager.h"
#include "time_manager.h"

void modePressed(void);
void setPressed(void);

#endif // PRESS_MANAGER_H_INCLUDED
