/*
 * state.h
 *
 *  Created on: 14 Sep 2014
 *      Author: Christ
 */

#ifndef STATE_H_
#define STATE_H_

#define MODE_TICK 0
#define MODE_SET 1
#define SET_SEC 0
#define SET_MIN 1
#define SET_HOUR 2
#define SET_DAY 3
#define SET_MNTH 4
#define SET_YEAR 5

unsigned char getSetterDisplayState(void);
void setSetterDisplayState(unsigned char _state);

unsigned char getSetterTriggerState(void);
void setSetterTriggerState(unsigned char _state);

unsigned char getMainState(void);
void setMainState(unsigned char _state);


#endif /* STATE_H_ */
