/*
 * set_display.h
 *
 *  Created on: 13 Sep 2014
 *      Author: Christ
 */

#ifndef SET_MANAGER_H_
#define SET_MANAGER_H_

#include "simple_time.h"

void increaseSecond(Date *_date);
void increaseMinute(Date *_date);
void increaseHour(Date *_date);
void increaseDay(Date *_date);
void increaseMonth(Date *_date);
void increaseYear(Date *_date);

void verifySecond(Date *_date);
void verifyMinute(Date *_date);
void verifyHour(Date *_date);
void verifyDay(Date *_date);
void verifyMonth(Date *_date);
void verifyYear(Date *_date);

void increaseParam(unsigned int * _param, int _minParamValue, unsigned int _maxParamValue);
void verifyParam(unsigned int * _param, int _minParamValue, unsigned int _maxParamValue);

#endif /* SET_MANAGER_H_ */
