/*
 * time_updater.c
 *
 *  Created on: Sep 12, 2014
 *      Author: Christ
 */

#include "time_manager.h"

static Date sDate;

void updateTick() {
    // tick, increase time
    increaseTime(&sDate);
}

void setDate(Date _date) {
    copyDate(&sDate, _date);
}

Date * getDatePointer() {
    return &sDate;
}

unsigned int getSecond() {
    return sDate.second;
}

unsigned int getMinute() {
    return sDate.minute;
}

unsigned int getHour() {
    return sDate.hour;
}

unsigned int getDay() {
    return sDate.day;
}

unsigned int getMonth() {
    return sDate.month;
}

unsigned int getYear() {
    return sDate.year;
}
