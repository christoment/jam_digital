/*
 * File:   test_state.c
 * Author: Christ
 *
 * Created on 22/09/2014, 12:35:19 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "state.h"

/*
 * CUnit Test Suite
 */

#define STATE_TEST_CONSTANT 1

int init_suite(void) {
    return 0;
}

int clean_suite(void) {
    return 0;
}

void testMainState() {
    // set main state
    setMainState(STATE_TEST_CONSTANT);
    
    unsigned char result = getMainState();
    
    CU_ASSERT(result == STATE_TEST_CONSTANT);
}

void testSetterDisplayState() {
    // set main state
    setSetterDisplayState(STATE_TEST_CONSTANT);
    
    unsigned char result = getSetterDisplayState();
    
    CU_ASSERT(result == STATE_TEST_CONSTANT);
}

void testSetterTriggerState() {
    // set main state
    setSetterTriggerState(STATE_TEST_CONSTANT);
    
    unsigned char result = getSetterTriggerState();
    
    CU_ASSERT(result == STATE_TEST_CONSTANT);
}

int main() {
    CU_pSuite pSuite = NULL;

    /* Initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_state", init_suite, clean_suite);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "testMainState", testMainState)) ||
            (NULL == CU_add_test(pSuite, "testSetterDisplayState", testSetterDisplayState)) ||
            (NULL == CU_add_test(pSuite, "testSetterTrigerState", testSetterTriggerState))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
