/*
 * File:   test_simple_time.c
 * Author: Christ
 *
 * Created on 22/09/2014, 12:44:17 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "simple_time.h"

/*
 * CUnit Test Suite
 */

#define TEST_SECOND 1
#define TEST_MINUTE 2
#define TEST_HOUR 3
#define TEST_DAY 4
#define TEST_MONTH 5
#define TEST_YEAR 6

    
int init_suite(void) {    
    return 0;
}

int clean_suite(void) {
    return 0;
}

void testCopyDate() {
    Date dateTarget;
    Date dateSource;

    dateSource.second = TEST_SECOND;
    dateSource.minute = TEST_MINUTE;
    dateSource.hour = TEST_HOUR;
    dateSource.day = TEST_DAY;
    dateSource.month = TEST_MONTH;
    dateSource.year = TEST_YEAR;
    
    copyDate(&dateTarget, dateSource);

    CU_ASSERT(
            (dateTarget.second == TEST_SECOND) &&
            (dateTarget.minute == TEST_MINUTE) &&
            (dateTarget.hour == TEST_HOUR) &&
            (dateTarget.day == TEST_DAY) &&
            (dateTarget.month == TEST_MONTH) &&
            (dateTarget.year == TEST_YEAR));
}

void testIncreaseTime() {
    Date dateSource;

    // check normal increment
    dateSource.second = 0;
    dateSource.minute = 0;
    dateSource.hour = 0;
    dateSource.day = 0;
    dateSource.month = 0;
    dateSource.year = 2000;
    
    increaseTime(&dateSource);
    
    CU_ASSERT(dateSource.second == 1);
    
    
}

void testNewDate() {
    unsigned char _second;
    unsigned char _minute;
    unsigned char _hour;
    unsigned char _day;
    unsigned char _month;
    unsigned int _year;
    Date result = newDate(_second, _minute, _hour, _day, _month, _year);
    if (1 /*check result*/) {
        CU_ASSERT(0);
    }
}

int getDayOfMonthCount(unsigned int _month, unsigned int _year);

void testGetDayOfMonthCount() {
    unsigned int _month;
    unsigned int _year;
    int result = getDayOfMonthCount(_month, _year);
    if (1 /*check result*/) {
        CU_ASSERT(0);
    }
}

int main() {
    CU_pSuite pSuite = NULL;

    /* Initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    /* Add a suite to the registry */
    pSuite = CU_add_suite("test_simple_time", init_suite, clean_suite);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "testCopyDate", testCopyDate)) ||
            (NULL == CU_add_test(pSuite, "testIncreaseTime", testIncreaseTime)) ||
            (NULL == CU_add_test(pSuite, "testNewDate", testNewDate)) ||
            (NULL == CU_add_test(pSuite, "testGetDayOfMonthCount", testGetDayOfMonthCount))) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}
