#include "press_manager.h"

void modePressed() {
    // toggle mode

    unsigned short int state = getMainState();
    switch (state) {
        case MODE_TICK:
            // switch to set
            setMainState(MODE_SET);
            setSetterDisplayState(SET_YEAR);
            break;
        case MODE_SET:
            // year -> month -> day -> hour -> min -> sec
            switch (getSetterDisplayState()) {
                case SET_YEAR:
                    setSetterDisplayState(SET_MNTH);
					// also verify month
					verifyMonth(getDatePointer());
                    break;
                case SET_MNTH:
                    setSetterDisplayState(SET_DAY);
					// also verify day
					verifyDay(getDatePointer());
                    break;
                case SET_DAY:
                    setSetterDisplayState(SET_HOUR);
					// also verify month
					verifyHour(getDatePointer());
                    break;
                case SET_HOUR:
                    setSetterDisplayState(SET_MIN);
					// also verify month
					verifyMinute(getDatePointer());
                    break;
                case SET_MIN:
                    setSetterDisplayState(SET_SEC);
					// also verify month
					verifySecond(getDatePointer());
                    break;
                case SET_SEC:
                    setMainState(MODE_TICK);
                    break;
            }
            break;
    }
}

void setPressed() {
    // toggle mode
    unsigned short int state = getMainState();
    switch (state) {
        case MODE_TICK:
            // no do?
            break;
        case MODE_SET:
            setSetterTriggerState(1);
            break;
    }
}
