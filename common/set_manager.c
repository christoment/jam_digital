/*
 * set_display.c
 *
 *  Created on: 13 Sep 2014
 *      Author: Christ
 */

#include "set_manager.h"

#define MIN_SECOND 0
#define MIN_MINUTE 0
#define MIN_HOUR 0
#define MIN_DAY 1
#define MIN_MONTH 1
#define MIN_YEAR 1970

#define MAX_SECOND 59
#define MAX_MINUTE 59
#define MAX_HOUR 23
#define MAX_MONTH 12
#define MAX_YEAR 2099

void increaseSecond(Date *_date) {
    increaseParam(&(*_date).second, MIN_SECOND, MAX_SECOND);
}

void increaseMinute(Date *_date) {
    increaseParam(&(*_date).minute, MIN_MINUTE, MAX_MINUTE);
}

void increaseHour(Date *_date) {
    increaseParam(&(*_date).hour, MIN_HOUR, MAX_HOUR);
}

void increaseDay(Date *_date) {
    increaseParam(&(*_date).day, MIN_DAY, getDayOfMonthCount((*_date).month, (*_date).year));
}

void increaseMonth(Date *_date) {
    increaseParam(&(*_date).month, MIN_MONTH, MAX_MONTH);
}

void increaseYear(Date *_date) {
    increaseParam(&(*_date).year, MIN_YEAR, MAX_YEAR);
}

void verifySecond(Date *_date) {
    verifyParam(&(*_date).second, MIN_SECOND, MAX_SECOND);
}

void verifyMinute(Date *_date) {
    verifyParam(&(*_date).minute, MIN_MINUTE, MAX_MINUTE);
}

void verifyHour(Date *_date) {
    verifyParam(&(*_date).hour, MIN_HOUR, MAX_HOUR);
}

void verifyDay(Date *_date) {
    verifyParam(&(*_date).day, MIN_DAY, getDayOfMonthCount((*_date).month, (*_date).year));
}

void verifyMonth(Date *_date) {
    verifyParam(&(*_date).month, MIN_MONTH, MAX_MONTH);
}

void verifyYear(Date *_date) {
    verifyParam(&(*_date).year, MIN_YEAR, MAX_YEAR);
}

void increaseParam(unsigned int * _param, int _minParamValue, unsigned int _maxParamValue) {
    (*_param)++;
    if ((*_param) > _maxParamValue) {
        (*_param) = _minParamValue;
    }
}

void verifyParam(unsigned int * _param, int _minParamValue, unsigned int _maxParamValue) {
    if ((*_param) > _maxParamValue) {
        (*_param) = _maxParamValue;
    } else if ((*_param) < _minParamValue) {
		(*_param) = _minParamValue;
	}
}
