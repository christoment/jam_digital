/*
 * display_updater.h
 *
 *  Created on: Sep 12, 2014
 *      Author: Christ
 */

#ifndef TIME_UPDATER_H_
#define TIME_UPDATER_H_

#include "simple_time.h"

void updateTick(void);

void setDate(Date _date);
Date * getDatePointer(void);

unsigned int getSecond(void);
unsigned int getMinute(void);
unsigned int getHour(void);
unsigned int getDay(void);
unsigned int getMonth(void);
unsigned int getYear(void);

#endif /* TIME_UPDATER_H_ */
