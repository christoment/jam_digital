/*
 * main.c
 *
 *  Created on: 13 Sep 2014
 *      Author: Christ
 */
#include "state_manager.h"

void setupState() {
    setMainState(MODE_TICK);
    setSetterDisplayState(SET_YEAR);
    setSetterTriggerState(0);
}

void startManageState(Date * date) {
    // read mode
    unsigned short int state = getMainState();
    switch (state) {
        case MODE_TICK:
            break;
        case MODE_SET:
            switch (getSetterDisplayState()) {
                case SET_SEC:
                    if (getSetterTriggerState()) {
                        increaseSecond(date);
                        setSetterTriggerState(0);
                    }
                    break;
                case SET_MIN:
                    if (getSetterTriggerState()) {
                        increaseMinute(date);
                        setSetterTriggerState(0);
                    }
                    break;
                case SET_HOUR:
                    if (getSetterTriggerState()) {
                        increaseHour(date);
                        setSetterTriggerState(0);
                    }
                    break;
                case SET_DAY:
                    if (getSetterTriggerState()) {
                        increaseDay(date);
                        setSetterTriggerState(0);
                    }
                    break;
                case SET_MNTH:
                    if (getSetterTriggerState()) {
                        increaseMonth(date);
                        setSetterTriggerState(0);
                    }
                    break;
                case SET_YEAR:
                    if (getSetterTriggerState()) {
                        increaseYear(date);
                        setSetterTriggerState(0);
                    }
                    break;

            }
            break;
    }

}
