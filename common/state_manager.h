/*
 * state_manager.h
 *
 *  Created on: 15 Sep 2014
 *      Author: Christ
 */

#ifndef STATE_MANAGER_H_
#define STATE_MANAGER_H_

#include "simple_time.h"
#include "set_manager.h"
#include "state.h"

void setupState(void);

void startManageState(Date * date);

#endif /* STATE_MANAGER_H_ */
