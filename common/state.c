/*
 * state.c
 *
 *  Created on: 14 Sep 2014
 *      Author: Christ
 */

#include "state.h"

static unsigned char sSetterDisplayState;
static unsigned char sSetterTriggerState;
static unsigned char sMainState;

unsigned char getSetterDisplayState() {
    return sSetterDisplayState;
}

void setSetterDisplayState(unsigned char _state) {
    sSetterDisplayState = _state;
}

unsigned char getSetterTriggerState() {
    return sSetterTriggerState;
}

void setSetterTriggerState(unsigned char _state) {
    sSetterTriggerState = _state;
}

unsigned char getMainState() {
    return sMainState;
}

void setMainState(unsigned char _state) {
    sMainState = _state;
}
